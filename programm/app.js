var request = require("request");
var cheerio = require("cheerio");
var fs = require('fs');
var max = 0;
var zahl = 1;
var streamcloud_links = [];
var user;

var xd = [];
var url = "http://bs.to";

var exec = require('child_process').exec;
exec('title Link Grabber v0.1', function (error, stdout, stderr) {
  // output is in stdout
});




var stream_service = {
	"streamcloud":
		['Streamcloud-1','streamcloud.eu'],
	"firedrive":
		['Firedrive-1','www.firedrive.com'],	
	"nowvideo":
		['NowVideo-1','nowvideo.ch']
	};

var arg = [];
process.argv.forEach(function (val, index, array) {
  arg[index] = val;
});
console.log('|_ Start App');
var u = arg[2];
if(u !== "webinterface"){
	var streamservice = arg[3];
	console.log(streamservice);
	if( streamservice === ''){
		streamservive = "streamcloud";
	}
	if( streamservice === '1'){
		streamservice = "streamcloud";
	}else if(streamservice === '2'){
		streamservice = "firedrive";
	}else if(streamservice === '3'){
		streamservice = "nowvideo";
	}
	console.log(stream_service[streamservice][0] +' '+ stream_service[streamservice][1]);
	sleep(1000);
	var link = u;
	init(link,streamservice);
}
else{
	//webinterface
	console.log('|_ Start WEB GUI');
	console.log('|_ open it with 127.0.0.1:32542');
	var express = require('express')
	, stylus = require('stylus')
	, nib = require('nib')	 
	var app = express();
	var http = require('http').Server(app);
	var io = require('socket.io')(http);
	var server = app.listen(32542, function(){
		var exec = require('child_process').exec;
		exec('start http://127.0.0.1:32542', function (error, stdout, stderr) {
		  // output is in stdout
		});
	});
	var io = require('socket.io').listen(server);
	var bodyParser = require('body-parser')
	app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
	  extended: true
	})); 
	function compile(str, path) {
	  return stylus(str)	
		.set('filename', path)
		.use(nib())
	}
	app.set('views', __dirname + '/webgui/views')
	app.set('view engine', 'jade')
	app.use(stylus.middleware(
	  { src: __dirname + '/webgui/public'
	  , compile: compile
	  }
	))
	io.on('connection', function(socket){
		console.log("|- You Connected");
		console.log(fs.readdirSync("../grab/"));
		user = socket;
		socket.on('getLink', function(data) {
			//console.log(data);
			if(data[0] !== ''){
				init(data[0],data[1]);
			}
		});
		socket.on('loadGrabbed', function(data) {
			socket.emit("grabbed",fs.readdirSync("../grab/"));
		});
		socket.on('loadTXT',function(file){
			var content;
			fs.readFile('../grab/'+file,{ encoding: 'utf8' }, function read(err, data) {
				if (err) {
					throw err;
				}
				content = data;
				socket.emit("grabbedLinksFromText",content);
			});
		});
		socket.on('deleteText',function(file){
			fs.unlinkSync('../grab/'+file);
			socket.emit("grabbed",fs.readdirSync("../grab/"));
		});
		socket.on('disconnect', function(){
		 
		});
	});
	app.use(express.static(__dirname + '/webgui/public'));
	app.get('/', function (req, res) {
	  res.render('index',
	  { title : 'Home' }
	  )
	})
	app.post('/grabLink', function(req,res){
		var link = req.body.link;
		res.writeHead(200, { 'Content-Type': 'text/plain' });
		res.end(link);
	})
}

function init(linkv,streamservice){
	max = 0;
	zahl = 1;
	streamcloud_links = [];
	xd=[];
	request({
		uri: linkv,
		}, function(error, response, body) {
		var $ = cheerio.load(body);
		fs.writeFile("log_site_output/"+encodeURIComponent(linkv)+".txt", body, function(err) {
			if(err) {
				console.log(err);
			} else {
				console.log("|- File saved");
			}
		}); 
		$("a").each(function() {
			var link = $(this);
			var text = link.text();
			var href = link.attr("href");
			var str = url  + href;
			var pieces = str.split("/");
			if(pieces[pieces.length-1]===stream_service[streamservice][0]){
				max++;
			}
		});
		console.log(max);
		$("a").each(function() {
			var link = $(this);
			var text = link.text();
			var href = link.attr("href");
			var str = url  + href;
			var pieces = str.split("/");
			if(pieces[pieces.length-1]===stream_service[streamservice][0]){
				console.log("|-- "+zahl +" "+url+href);
				xd.push(url+'/'+href);
				zahl = zahl + 1;
				sleep(50);
				if(zahl > max){
					console.log("|-------------------------");
					console.log('|- Start grabbing links');
					console.log('|- Found '+xd.length+' links');
					console.log("|-------------------------");
					sleep(1000);
					console.log("|- Start");
					start_real(linkv,streamservice);
				}
			}	
		});
	});
}
function start_real(strs,streamservice){
	var num = 1;
	x = setTimeout(function(){
		if(num < xd.length){
			console.log('|- Failed try again');
			console.log('|- Restart');
			start_real(str,streamservice);
		}
	},90000);
	for(var z = 0; z<xd.length;z++){
		request({
			uri: xd[z],
			}, function(error, response, body) {
				/*
				fs.writeFile("log_site_output/"+encodeURIComponent(xd[z])+".txt", body, function(err) {
					if(err) {
						console.log(err);
					} else {
						console.log("|- File saved");
					}
				}); 
				sleep(200);*/
				var cheerio2 = require("cheerio");
				var $ = cheerio2.load(body); 
				$("a").each(function() {
					var link = $(this);
					var text = link.text();
					var hrefs = link.attr("href");
					var str = hrefs;
					var pieces = str.split("/");
					if(pieces[2] === stream_service[streamservice][1] && text === "" ){
						console.log("|--"+num +"	:"+ text+hrefs);
						streamcloud_links.push(hrefs);
						num++;
						if(num > max){
							console.log("|- Wait ... 5sec ...");
							setTimeout(function(){
								sleep(500);
								clearTimeout(x);
								console.log("|- Timeout closed");
								savefile(strs,streamservice);
								console.log("|----------------------------");
							},5000);
							setTimeout(function(){
								console.log("|- Wait ... 2sec ...");
							},3000);
						}
						
					}
				});
				sleep(10);
		});
		sleep(10);
	};
}
function savefile(link,streamservice){
	var file_name="";
	var txt_name = link.split("/");
	var text = "";
	for(tx = 4; tx < txt_name.length;tx++){
		file_name = file_name+'_'+txt_name[tx];
	}
	for(s_l = 0; s_l < streamcloud_links.length;s_l++){
		text = text + streamcloud_links[s_l]+'\n';
		if(u === "webinterface"){
			user.emit("row",streamcloud_links[s_l]+'\n');
		}
	}
	fs.writeFile("../grab/"+file_name+' '+stream_service[streamservice][1]+'.txt', text, function(err) {
		if(err) {
			console.log(err);
		} else {
			console.log("|- File saved");
		}
	}); 
}
function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
	if ((new Date().getTime() - start) > milliseconds){
	  break;
	}
  }
}