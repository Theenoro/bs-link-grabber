var socket = io();
function loadLink(){
	var data = [$('#link').val(),$('#stream').val()];
	$("#getlink").attr("disabled", true);   
	$("#state").html("Loading do not reload the page!!");
	$("#field").html("");
	socket.emit('getLink', data);
};
socket.on('row', function(text) {
	$("#state").html("finished");
	$("#field").append(text+'<br>');
	$("#getlink").removeAttr("disabled");   
});
socket.on("grabbed", function(data) {
	$("#list").html("");   
	console.log("XD");
	data.forEach(function(entry) {
		$("#list").append("<div style='display:inline-block;' class='loadTEXT' file='"+entry+"'>"+entry+"</div><button class='delete' file='"+entry+"'>DELETE</button>"+'<br>'); 
	});	
	$('.loadTEXT').click(function(){
		var f = $(this).attr('file');
		$('.menu').append('<span id="file"> \< '+f+'</span>');
		console.log("XD"+f);
		socket.emit('loadTXT', f);
	});
	$('.delete').click(function(){
		var f = $(this).attr('file');
		socket.emit('deleteText', f);
	});
});
socket.on('grabbedLinksFromText', function(text) {
	$("#list").html("");   
	$("#list").append(text); 
});
$(document).ready(function(){
	$(".main-content").load( "/html/getLinks.html", function() {
	});
	$("#already").click(function(){
		$(".main-content").load( "/html/grabbed.html", function() {
			socket.emit('loadGrabbed', "");
			$( "#file" ).remove();
		});
	});
	$("#grabber").click(function(){
		$(".main-content").load( "/html/getLinks.html", function() {
		});
	});
});

//grabbedLinksFromText
//https://www.burning-seri.es/serie/Heaven-s-Memo-Pad